# DATOS EXTRA #

* He probado con varios rss y funciona, lo unico que solo puedes ver una categoria

* Si alguna vez te pega un chascazo al iniciar y no te sale el Menu, cancela y vuelve a ejecutar, no se si solo es en mi pc, pero cuando pasa eso tenía que eliminar la bbdd del sql y volver a importarla con la opcion 0 del menu.

* En un principio tenia la tabla n-n de CategoriasPost, pero debido a que el programa no puede leer mas de una categoria la he dejado comentada y he modificado las tablas para que solo lean una categoria.

* El nombre que saldrá en las categorias de los post será el de Frikismo, que es el nombre que le he dado a la de por defecto y los de tu rss los convertirá al importarlo.

* Ya que me diste luz verde he decidido dejar la fecha como varchar

* Se que esto es inmensamente mejorable pero al menos hace lo que pides.


* TRIGGER:

--PARTIMOS DE QUE SI NO TIENE CATEGORIA ES UNCATEGORIZED 

-- el trigger funciona cuando se agrega un post, ya que el post lee del rss la categoria. La tabla categorias estaría previamente creada.
-- si encuentra un post uncategorized crea la categoria FRIKISMO si no existia ya y mete ahi el post.
-- En uncategorized no saldrá ningun post aunque exista como categoría
CREATE TRIGGER defaultCategories2
ON post
FOR insert
AS 
BEGIN
	SET NOCOUNT ON;
	declare @nuevonombre as varchar(500)
	set @nuevonombre='Frikismo'
	declare @nombres as varchar(500)
	set @nombres=(select c.nombre from inserted i inner join categoria c on c.ID=i.ID_Categoria)
	IF (@nombres='uncategorized')
		BEGIN	
			IF NOT EXISTS (SELECT nombre FROM Categoria WHERE nombre ='Frikismo') begin INSERT INTO Categoria(nombre) values('Frikismo') end
			update post set ID_Categoria=(select ID from Categoria where nombre='Frikismo') 

		   
		END
END



