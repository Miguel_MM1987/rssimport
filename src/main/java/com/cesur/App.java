package com.cesur;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Hello bernat!
 *
 */
public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="rssimport";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    /** 
     * @param args
     * @throws SQLException
     * @throws ClassNotFoundException
     */

  
    public static void main( String[] args ) throws ClassNotFoundException, SQLException {
    
     
      bbdd rss = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        XmlParse x = new XmlParse();
        String url = "https://lenguajedemarcasybbdd.wordpress.com/feed/";
       
        
       int opcion;
       do {		
       pintarMenu();			
       System.out.println("Introduce la opción deseada:");
        opcion = pedirOpcion(0, 6);
        switch (opcion) {
        case 0:
            importarRSS(rss, x, url);
            System.out.println("Solicitud ejecutada correctamente. Volviendo al Menu Principal...");
            break;
        
        case 1:            
            do {
                mostrarTodos(rss);
                System.out.println("\nQuieres seguir usando este Menu? Pulsa 1 para SI o 2 para VOLVER al Menu Principal");
                opcion = pedirOpcion(1, 2);
            } while (opcion != 2);            
            break;
        case 2:            
            do {
                ultimos10(rss);   
                System.out.println("\nQuieres seguir usando este Menu? Pulsa 1 para SI o 2 para VOLVER al Menu Principal");
                opcion = pedirOpcion(1, 2);
            } while (opcion != 2);         
            break;
        case 3:         
            do {
                detallePost(rss);   
                System.out.println("\nQuieres seguir usando este Menu? Pulsa 1 para SI o 2 para VOLVER al Menu Principal");
                opcion = pedirOpcion(1, 2);
            } while (opcion != 2);               
            break;
        case 4:            
            do {
                filtrarCategoria(rss);  
                System.out.println("\nQuieres seguir usando este Menu? Pulsa 1 para SI o 2 para VOLVER al Menu Principal");
                opcion = pedirOpcion(1, 2);
            } while (opcion != 2); 
            break;
        case 5:    
        do {
            buscador(rss);    
            System.out.println("\nQuieres seguir usando este Menu? Pulsa 1 para SI o 2 para VOLVER al Menu Principal");
            opcion = pedirOpcion(1, 2);
        } while (opcion != 2);              
        break;
        
        default:
            System.out.println("Gracias por usar el Sistema de consultas de RSS");
        }
    } while (opcion >= 0 && opcion <= 5);

    }

    private static void CrearBBDD(bbdd rss)
    {
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       // String cero = "0";
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");
          
            b.modificarBBDD(query);  
            rss.modificarBBDD("CREATE TRIGGER defaultCategories2 ON post FOR insert AS BEGIN	SET NOCOUNT ON;	declare @nuevonombre as varchar(500)	set @nuevonombre='Frikismo'	declare @nombres as varchar(500)	set @nombres=(select c.nombre from inserted i inner join categoria c on c.ID=i.ID_Categoria)	IF (@nombres='uncategorized')		BEGIN				IF NOT EXISTS (SELECT nombre FROM Categoria WHERE nombre ='Frikismo') begin INSERT INTO Categoria(nombre) values('Frikismo') end			update post set ID_Categoria=(select ID from Categoria where nombre='Frikismo') 		   		END END     ");
      
        }
    }

    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }

    public static void importarRSS(bbdd rss, XmlParse x, String url){
        CrearBBDD(rss);
      
        List<String>  titulos = x.leer("//item/title/text()",url);
        List<String> postpubDate = x.leer("//item/pubDate/text()", url);
        List<String> postLink = x.leer("rss/channel/item/link/text()", url);
        List<String> postContenido = x.leer("rss/channel/item/encoded/text()", url);
        
        List<String> postCategorias = x.leer("rss/channel/item/category/text()",url);
        List<String>  autores = x.leer("//item/creator/text()",url);
        
        
        for (int i = 0; i < titulos.size(); ++i) {
            rss.modificarBBDD("IF NOT EXISTS (SELECT 1 FROM Categoria WHERE nombre ='"+postCategorias.get(i)+"')BEGIN INSERT INTO Categoria(nombre) VALUES ('"+postCategorias.get(i)+"') END");
            rss.modificarBBDD("IF NOT EXISTS (SELECT 1 FROM Usuario WHERE nombre ='"+autores.get(i)+"')BEGIN INSERT INTO Usuario(nombre) VALUES ('"+autores.get(i)+"') END");
         
         //rellenar tabla post
              
           rss.modificarBBDD("insert into post(titulo,pubDate,enlace,contenido,ID_categoria,ID_Usuario) values ('"+titulos.get(i)+"','"+postpubDate.get(i)+"','"+postLink.get(i)+"','"+postContenido.get(i)+"',(SELECT id FROM Categoria WHERE nombre ='"+postCategorias.get(i)+"'),(SELECT id FROM Usuario WHERE nombre ='"+autores.get(i)+"'))");
           
     
         }
        
    }

    public static void mostrarTodos(bbdd rss){
     rss.leerBBDD("select id,pubdate,titulo from post", 3);

    }

    public static void ultimos10(bbdd rss){
        rss.leerBBDD("select top 10 id,pubdate,titulo from post order by pubDate desc", 3);
    }

    public static void detallePost(bbdd rss){
       System.out.println("inserta el id del post");
        Scanner sc=new Scanner(System.in);
        int idScanner=sc.nextInt();
        sc.nextLine();
        System.out.println("\nTitulo:");
        rss.leerBBDD("select titulo from post where id='"+idScanner+"'",1);
        System.out.println("_______________________________");
        System.out.println("Fecha de Publicación:");
        rss.leerBBDD("select pubDate from post where id='"+idScanner+"'",1);
        System.out.println("_______________________________");
        System.out.println("URL:");
        rss.leerBBDD("select enlace from post where id='"+idScanner+"'",1);
        System.out.println("_______________________________");
        System.out.println("Contenido:");
        rss.leerBBDD("select contenido from post where id='"+idScanner+"'",1);
        System.out.println("_______________________________");
        System.out.println("Autor:");
        rss.leerBBDD("select nombre from Usuario where id=(select ID_Usuario from Post where id='"+idScanner+"')",1);
        System.out.println("_______________________________");
        System.out.println("Categoria:");
        rss.leerBBDD("select nombre from categoria where id=(select ID_Categoria from Post where id='"+idScanner+"')",1);
        System.out.println("_______________________________\n");
    }

    public static void filtrarCategoria(bbdd rss){
        //Mostramos las categorias
        rss.leerBBDD("select id,nombre from Categoria",2);
        
        System.out.println("Introduce el id de la categoria");
        Scanner sc=new Scanner(System.in);
        String cat=sc.next();
        System.out.println("\nPost:");
        rss.leerBBDD("select p.id,p.titulo,p.pubdate from post  p   inner join Categoria c on c.ID=p.ID_Categoria     where c.id='"+cat+"'",3);
        System.out.println("_______________________________");
    }

    public static void buscador(bbdd rss) throws SQLException, ClassNotFoundException{
               
        System.out.println("Pulsa 1 para buscar en el contenido o 2 para buscar en el titulo del post");
        int opcion=pedirOpcion(1, 2);
        Scanner scBuscador=new Scanner(System.in);
        String texto;
        if (opcion==1){
            System.out.println("Introduce la palabra que quieres buscar en el contenido del post");
            texto=scBuscador.next();                   
            rss.leerBBDD("select ID,pubDate,titulo from Post where contenido like '%"+texto+"%'", 3);
                       
        }else{
            System.out.println("Introduce la palabra que quieres buscar en el título");
            texto=scBuscador.next();
            rss.leerBBDD("select ID,pubDate,titulo from Post where titulo like '%"+texto+"%'", 3);
        }
    }

    public static int pedirOpcion(int desde, int hasta) {
		int x = 0;
		try {
		Scanner opt=new Scanner(System.in);
		
        do {
            x = opt.nextInt();
            if (x < desde || x > hasta) {
                System.out.println("Debe estar entre " + desde + " y " + hasta);
            }
        } while (x < desde || x > hasta);
        
    } catch (InputMismatchException ex) {
		System.out.println("Debes ingresar obligatoriamente un número.");
	}finally{
		return x;
	}
        
	}

    public static void pintarMenu() {
        System.out.println("+----------------------------------------------------+");
		System.out.println("|Bienvenido al Programa de Gestión de RSS|");
		System.out.println("+----------------------------------------------------+");
		System.out.println("Elige una opción.");
		System.out.println("Si es la primera vez que utilizas este programa, utiliza la opción 0 para crear Y rellenar las Tablas de la BBDD.");
		System.out.println("+----------------------------------------+");
		System.out.println("|Cración e Insercion en TABLAS de tu Base de Datos|");
		System.out.println("+----------------------------------------+\n");
		System.out.println("0. Crea y rellena tu BBDD de RSS.\n");		
		System.out.println("+----------------------------------------+");
		System.out.println("|Opciones de gestión sobre tu Base de datos|");
		System.out.println("+----------------------------------------+");
		System.out.println("1. Mostrar por consola todos  los post.\n");
		System.out.println("2. Mostrar los últimos 10 post.\n");
		System.out.println("3. Mostrar un post al detalle.\n");
        System.out.println("4. Filtrar post por categoria.\n");
		System.out.println("5. Buscador en contenido o título.\n");
		System.out.println("6. Salir.\n");
    }


}




